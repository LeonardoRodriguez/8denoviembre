<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of SolicitudController
 *
 * @author a071058144n
 */
class SolicitudController extends BaseController{
    public function formDatos() {
        $data['title'] = 'Formulario Datos';
        return view ('solicitud/datos', $data);
    }
}
