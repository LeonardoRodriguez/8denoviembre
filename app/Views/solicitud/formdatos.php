<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <h1 class="Datos"><?= $title?></h1>
        <form action="<?= site_url('solicitud/formdatos')?>" method="post">
            <div class="form-group">
                <label for="NIA">NIA</label>
            <input type="text" name="NIA" value="" id= "NIA" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="nombre">nombre</label>
            <input type="text" name="nombre" value="" id= "nombre" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="apellido1">apellido1</label>
            <input type="text" name="apellido1" value="" id= "apellido1" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="apellido2">apellido2</label>
            <input type="text" name="apellido2" value="" id= "apellido2" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="NIF">NIF</label>
            <input type="text" name="NIF" value="" id= "NIF" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="ciclo">ciclo</label>
            <input type="text" name="ciclo" value="" id= "ciclo" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="tipo_tasa">tipo_tasa</label>
            <input type="text" name="tipo_tasa" value="" id= "tipo_tasa" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="email">email</label>
            <input type="text" name="email" value="" id= "email" class="form-control"/>
            </div>
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>
